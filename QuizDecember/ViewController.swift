//
//  ViewController.swift
//  QuizDecember
//
//  Created by Nyta on 12/9/20.
//

import UIKit



class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var user : [Info] = []
    var swaps = [Info]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let uri = "https://reqres.in/api/users?page=2"
        let url: URL = URL(string: uri)!
        
                var request = URLRequest(url: url)
        
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")

                URLSession.shared.dataTask(with: request) {
                    (data, response, error) in
                    
                    if let err = error {
                        print(err)
                        
                    }
                    guard let data = data else {
                        return
                        
                    }
                    
                    do {
                        
                        let response = try JSONDecoder().decode(Person.self, from: data)
                        for users in response.data {

                            self.swaps.append(users)
                        }

                    } catch let error {
                        print(error)
                    }
                    
                }.resume()

            
            }
      
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return swaps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.detailTextLabel?.text = swaps[indexPath.row].email
        
        cell.textLabel?.text = " \(swaps[indexPath.row].first_name)\(swaps[indexPath.row].last_name)" 
        
        let url = URL(string: "\(swaps[indexPath.row].avatar)")
        let dataImage = try? Data(contentsOf: url!)
        let cellImage = UIImage(data: dataImage!)
        cell.imageView?.image = cellImage

        return cell
    }
    
   
}




