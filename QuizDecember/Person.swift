//
//  Person.swift
//  QuizDecember
//
//  Created by Nyta on 12/9/20.
//

import Foundation


struct Person: Codable{
    var data: [Info]
}


struct Info: Codable {
    
    let first_name: String
    let last_name: String
    let email: String
    let avatar: String
    let id: Int
    
}


